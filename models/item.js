const Recommendation = require('./recommendation');

class Item {
	constructor (name, description, tags) {
		this._id = 'I' + Item.ID;
		this.name = name;
		this.description = description;
		this._tags = new Set();
		this.tags = tags;
		this._recommendations = [];
	}

	get name () { return this._name;}

	set name (value) { this._name = value;}

	get description () { return this._description;}

	set description (value) { this._description = value;}

	get tags () { return this._tags;}

	set tags (values) {
		for (let tag of this._tags) {
			this.removeTag(tag);
		}
		for (let tag of values) {
			this.addTag(tag);
		}

	}

	removeTag (tag) {
		const tagSet = Item.byTags.get(tag) || new Set();
		tagSet.delete(this._id);
		Item.byTags.set(tag, tagSet);
		this._tags.delete(tag);
	}

	addTag (tag) {
		const tagSet = Item.byTags.get(tag) || new Set();
		tagSet.add(this._id);
		Item.byTags.set(tag, tagSet);
		this._tags.add(tag);
	}

	get recommendations () { return this._recommendations;}

	toJSON () {
		let score = {};
		let votes = this.recommendations.length;
		if (votes) {
			let sum = this.recommendations.reduce((a, b) => a + b.score, 0);
			let average = sum / (votes);
			score = {
				votes, average
			};
		}

		return {
			_id: this._id,
			name: this.name,
			description: this.description,
			tags: Array.from(this.tags),
			score,
			recommendations: this.recommendations
		};
	}


	recommends (author, message, score) {
		if (score >= 0 && score <= 10) {
			this.recommendations.push(new Recommendation(author, message, score));
		}
	}


	static get ID () {
		const _id = Item.__id;
		Item.__id++;
		return _id;
	}

	static create (name, description, tags) {
		let item = new Item(name, description, tags);

		Item.items.set(item._id, item);

		return item;
	}

	static update (id, fields) {
		const item = Item.get(id);
		for (let key of Object.keys(fields)) {
			const property = Object.getOwnPropertyDescriptor(Item.prototype, key);
			if (property.set) {
				item[key] = fields[key];
			}
		}
		return item;
	}


	static getAll () {
		return Array.from(Item.items.values());
	}

	static get (id) {
		const ret = Item.items.get(id);
		if (ret) return ret;
	}

	static getByTag (tags) {
		const ids = new Set();
		for (let tag of tags.split(',')) {
			const idsForTag = Item.byTags.get(tag);
			if (idsForTag)
				for (let id of idsForTag)
					ids.add(id)
		}

		return Array.from(ids, id => Item.get(id));
	}
}

Item.__id = 0;
Item.items = new Map();
Item.byTags = new Map();

module.exports = Item;